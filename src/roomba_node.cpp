#include <stdio.h>
#include <boost/asio.hpp>
#include <boost/asio/serial_port.hpp>
#include <inttypes.h>
#include <math.h>
#include <iostream>


#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

#define D2R(d) d*0.0174532925
//
// Need to write data to the serial port
// 
// Need to read data from the serial port

//Open port
//  - Set baud rate
//  - Set port address
typedef boost::shared_ptr<boost::asio::serial_port> serial_port_ptr;

class roombaSerial
{
    boost::asio::io_service         _io_service;
    serial_port_ptr                 _port;

    public:
    roombaSerial(const char *com_port_name, int baud_rate=115200)
    {
        boost::system::error_code ec;

        while (1) {
            std::cout << "Trying to find roomba" << std::endl;
            _port = serial_port_ptr(new boost::asio::serial_port( _io_service ));
            _port->open(com_port_name, ec);
        
            if (ec) {
                std::cout << "Can't find roomba retrying..." << std::endl;
                _port->close();
                sleep(3);
            } else {
                break;
            }

        }


        _port->set_option(boost::asio::serial_port_base::baud_rate(baud_rate));
        _port->set_option(boost::asio::serial_port_base::character_size(8));
        _port->set_option(boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::one));
        _port->set_option(boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::none));
        _port->set_option(boost::asio::serial_port_base::flow_control(boost::asio::serial_port_base::flow_control::none));
    
        sleep(1);

        return;
    }


    int write(unsigned char *buff, unsigned int size)
    {
        boost::system::error_code ec;
 
        if ((!_port) || (size == 0)) {
            std::cout << "error" << std::endl;
            return 0;
        }

        unsigned int res = _port->write_some(boost::asio::buffer(buff, size), ec);
        
        usleep(500000); // Default time needed between serial commands

        return res;
    }

    void close()
    {
        _port->cancel();
        _port->close();

    }

};




class Roomba: public roombaSerial
{
    public:

    Roomba(const char *port):
        roombaSerial (port)
    {
        std::cout << "Roomba setup" << std::endl;
    }

    void wake()
    {
        unsigned char buff[32] = "$$$S@,8080\nS&,8000\nS&,8080\n---\n";
        std::cout << "Wake Roomba" << std::endl;
        write(buff,  3);
        
        write(buff + 3,  8);
        usleep(50000);
        write(buff + 11, 8);
        usleep(50000);
        write(buff + 19, 8);
        
        write(buff + 27, 4);
        std::cout << "Awake" << std::endl;
    }

    void off()
    {
        unsigned char buff[1] = {133};
        
        std::cout << std::endl << "Roomba Off" << std::endl;
        write(buff, 1);
        close();
        
    }

    void move(const uint16_t trans, const float dtheta)
    {
        float radius;

        if ( ((dtheta > 0.01) || (dtheta < -0.01)) && dtheta != 0 ) 
            radius = ( trans/sinf( M_PI - 2 * ((M_PI/2) - dtheta)) ) * sinf((M_PI/2) - dtheta);
        else
            radius = 0x8000;


        uint16_t transl  = __builtin_bswap16(trans);
        uint16_t rotatel = __builtin_bswap16( (uint16_t) radius );


        unsigned char buff[5] = { 137, 0x00, 0x00, 0x00, 0x00 };
        memmove(buff+1, &transl,  2);
        memmove(buff+3, &rotatel, 2);
        
        write(buff, 5);
        
        std::cout << "Move " << trans << " " << radius << std::endl;
    } 

    void startControl()
    {
        unsigned char buff[2] = {128, 130};

        write(buff, 1);
        write(buff + 1, 1);

        std::cout << "Roomba is ready..." << std::endl;
    }

    
};





Roomba *roombaRef; // pointer to roomba object for callback functions

void roombaTwistControl(const geometry_msgs::Twist twist)
{
    std::cout << "Linear: " << twist.linear.x << " Angular: " << twist.angular.z << std::endl;

    roombaRef->move(twist.linear.x * 1000, twist.angular.z);
}


//
// Subscriber to listen for twist messages
//  - convert twist to roomba serial commands
// 
int main(int argc, char *argv[])
{
    ros::init(argc, argv, "roomba");
    ros::NodeHandle n;

    Roomba roomba("/dev/tty.RNBT-204A-RNI-SPP");
    roombaRef = &roomba;
    

    roomba.wake();
    roomba.startControl();

    ros::Subscriber roombaSubscriber = n.subscribe("cmd_vel", 1, roombaTwistControl);

    ros::spin();

    roomba.off();

    return 0;
}